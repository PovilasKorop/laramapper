<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\City;
use App\Store;
use Illuminate\Auth\Access\Gate;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class MapController extends Controller
{

    /**
     * Display a listing of Store.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request('show_deleted') == 1) {
            if (!Gate::allows('store_delete')) {
                return abort(401);
            }
            $stores = Store::onlyTrashed()->get();
        } else {
            $categories = Category::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
            $cities = City::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
            $stores = Store::with(['city:id,name', 'categories:id,name', 'media'])->get();
            $positions = DB::Table('stores')->selectRaw('min(address_latitude) as min_lat, min(address_longitude) as min_lng, 
                                                         max(address_latitude) as max_lat, max(address_longitude) as max_lng')->first();
            $default_center_latitude = (($positions->min_lat + $positions->max_lat) / 2);
            $default_center_longitude = (($positions->min_lng + $positions->max_lng) / 2);
            $default_zoom = 5;
        }

        return view('client.map', compact('stores', 'categories', 'cities', 'default_center_latitude', 'default_center_longitude', 'default_zoom'));
    }

}
