<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Admin\StoresController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\City;
use App\Store;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

    public function filter(Request $request, Store $store)
    {
        $categories = Category::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $cities = City::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');

        $category_id = $request->input('category_id');
        $city_id = $request->input('city_id');
        $search = $request->input('store_name');

        $stores = Store::with('city:id,name', 'categories:id,name', 'media')
            ->where(function ($q) use ($city_id, $category_id, $search) {
                if ($city_id) {
                    $q->where(function ($q2) use ($city_id) {
                        $q2->where('city_id', $city_id);
                    });
                }
                if ($category_id) {
                    $q->WhereHas('categories', function ($q2) use ($category_id) {
                        $q2->Where('category_id', $category_id);
                    });
                }
                if ($search) {
                    $q->where(function ($q2) use ($search) {
                        $q2->where('name', 'LIKE', "%$search%");
                    });
                }
            })->get();
        foreach ($stores as $key => $value) {
            $positions = (object)
            $pos ['lat'][] = $value->address_latitude;
            $pos ['lng'][] = $value->address_longitude;
            $positions->min_lat = min($pos['lat']);
            $positions->max_lat = max($pos['lat']);
            $positions->min_lng = min($pos['lng']);
            $positions->max_lng = max($pos['lng']);
        };

        if (isset($positions)) {
            $default_center_latitude = (($positions->min_lat + $positions->max_lat) / 2);
            $default_center_longitude = (($positions->min_lng + $positions->max_lng) / 2);
            if (count($stores) == 1 || $city_id) {
                $default_zoom = config('app.default_zoom');
            } else {
                $default_zoom = 5;
            }
        } else {
            $default_center_latitude = config('app.default_center_latitude');
            $default_center_longitude = config('app.default_center_longitude');
            $default_zoom = 5;
        }

        return view('client.map', compact('stores', 'categories', 'cities', 'default_center_latitude', 'default_center_longitude', 'default_zoom'));

    }
}
